<?php
/*
Plugin Name: woocomerce_fild
Plugin URI: http://wordpress.loc
Description: add field woocomerce
Version:Версии плагина: 1.0
Author: NoComments
Author URI: http://wordpress.loc
License: GPLv2
*/

add_action( 'woocommerce_after_order_notes', 'my_field_woocomerce' );
function my_field_woocomerce( $checkout ) {
    echo '<div id="my_custom_checkout_field"><h2>' . __('My field') . '</h2>';

    woocommerce_form_field( 'additional_information', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => __('Fill this field'),
        'placeholder'   => __('Enter the required text here'),
        ), $checkout->get_value( 'additional_information' ));
    echo '</div>';
}
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {
    // Проверяем, заполнено ли поле, если же нет, добавляем ошибку.
    if ( ! $_POST['additional_information'] )
        wc_add_notice( __( 'Please enter the required text in our new wonderful field' ), 'error' );
}


add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['additional_information'] ) ) {
        update_post_meta( $order_id, 'My Field', sanitize_text_field( $_POST['additional_information'] ) );
    }
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('My Field').':</strong> ' . get_post_meta( $order->id, 'My Field', true ) . '</p>';
}

/**
 * Добавляем поле в письма заказа
 **/

add_filter('woocommerce_email_order_meta_keys', 'my_custom_checkout_field_order_meta_keys');

function my_custom_checkout_field_order_meta_keys( $keys, $order ) {
    $keys['My Field'] = get_post_meta( $order->id, 'My Field', true );
    return $keys;
}